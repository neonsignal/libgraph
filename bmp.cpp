// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include "bmp.h"
#include <iostream>
#include <fstream>
#include <stdexcept>

namespace graph
{
// BMP structures
#pragma pack(1)
struct Bgr
{
	uint8_t blue;
	uint8_t green;
	uint8_t red;
};
struct Header
{
	uint16_t type;
	uint32_t sizeFile;
	uint32_t reserved;
	uint32_t offBits;
	uint32_t sizeHeader;
	int32_t nC, nR;
	uint16_t planes;
	uint16_t bitCount;
	uint32_t compression;
	uint32_t sizeImage;
	int32_t xpelsPerMetre, ypelsPerMetre;
	uint32_t clrUsed;
	uint32_t clrImportant;
};
#pragma pack()

// BMP load
void BmpImage::load(const char *filename)
{
	// open BMP file
	std::ifstream file(filename, std::ios::binary);

	// read header
	Header header;
	file.read(reinterpret_cast<char *>(&header), sizeof(header));

	// check for valid bitmap
	if (header.type != 'B'+('M'<<8))
		throw std::invalid_argument("file is not in BMP format");
	if (header.bitCount != 24)
		throw std::invalid_argument("BMP file is not 24 bit color");
	if (header.compression != 0)
		throw std::invalid_argument("BMP file is compressed");

	// initialize image parameters
	m_image.clear();

	// make line buffer
	Bgr *pLine = new Bgr[header.nC];

	// make image space
	Rgb rgb = {0,0,0};
	std::vector<Rgb> line(header.nR, rgb);
	m_image.insert(m_image.begin(), header.nC, line);

	// load bitmap data
	for (int r = header.nR; r > 0; --r)
	{
		file.read(reinterpret_cast<char *>(pLine), header.nC*sizeof(Bgr));
		char padding[4-1];
		file.read(padding, -header.nC*sizeof(Bgr)&4-1);
		for (int c = 0; c < header.nC; ++c)
		{
			m_image[c][r-1].red = pLine[c].red/255.;
			m_image[c][r-1].green = pLine[c].green/255.;
			m_image[c][r-1].blue = pLine[c].blue/255.;
		}
	}

	// delete line buffer
	delete[] pLine;

	// close bitmap
	file.close();
}

// BMP save
void BmpImage::save(const char *filename)
{

	// open BMP file
	std::ofstream file(filename, std::ios::binary);

	// calculate header parameters
	Header header;
	header.type = 'B'+('M'<<8);
	header.reserved = 0;
	header.offBits = sizeof(Header);
	header.sizeHeader = 40;
	header.planes = 1;
	header.bitCount = 24;
	header.compression = 0;
	header.nC = m_image.size();
	header.nR = m_image[0].size();
	header.sizeImage = (header.nC*sizeof(Bgr)+3&-4) * header.nR;
	header.xpelsPerMetre = 0;
	header.ypelsPerMetre = 0;
	header.clrUsed = 0;
	header.clrImportant = 0;
	header.sizeFile = header.offBits + header.sizeImage;

	// write header
	file.write(reinterpret_cast<char *>(&header), sizeof(header));

	// make line buffer
	Bgr *pLine = new Bgr[header.nC];

	// save bitmap data
	for (int r = header.nR; r > 0; --r)
	{
		for (int c = 0; c < header.nC; ++c)
		{
			pLine[c].red = static_cast<uint8_t>(m_image[c][r-1].red*255.);
			pLine[c].green = static_cast<uint8_t>(m_image[c][r-1].green*255.);
			pLine[c].blue = static_cast<uint8_t>(m_image[c][r-1].blue*255.);
		}
		file.write(reinterpret_cast<char *>(pLine), header.nC*sizeof(Bgr));
		char padding[4-1] = {0,0,0};
		file.write(padding, -header.nC*sizeof(Bgr)&4-1);
	}

	// delete line buffer
	delete[] pLine;

	// close bitmap
	file.close();
}
};
