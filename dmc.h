// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file dmc.h
	* DMC color functions.
	*/
#pragma once

// include files
#include "colors.h"
#include <vector>

namespace graph
{
typedef std::vector<std::vector<Rgb>> Image;

// colors
struct Color
{
	const char *number, *name;
	Rgb rgb;
};

// DMC filter class
class DmcImage
{
public:
	DmcImage(Image *pImage, uint dmcSet);
	void filter();
	void writeDmcFile();
private:
	int find(const Rgb &rgb);
	Image &m_image;
	Color *colorSet;
	uint colorSetLength;
};
};
