// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file pnm.h
	* PNM functions.
	*/
#pragma once

// include files
#include <vector>
#include "colors.h"

namespace graph
{
typedef std::vector<std::vector<Rgb>> Image;

// PNM image
class PnmImage
{
public:
	PnmImage(Image *pImage) : m_image(*pImage) {};
	void load(const char *filename);
	void save(const char *filename);
private:
	Image &m_image;
};
};
