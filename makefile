# definitions
LIBRARY = libgraph
SRC = colors.cpp transform.cpp bmp.cpp pnm.cpp dmc.cpp filter.cpp hilbert.cpp

# definitions
CPPFLAGS = -O2 @gcc.conf -MMD -MP
LDFLAGS = -O2

# all
all: $(LIBRARY).a html/index.html test

html/index.html: $(LIBRARY).a
	doxygen Doxyfile

clean:
	rm -f $(LIBRARY) *.o *.a

# library functions
$(LIBRARY).a: $(SRC:%.cpp=%.o)
	rm -f $@
	ar qc $@ $(SRC:%.cpp=%.o)

# library
test: test.o $(LIBRARY).a
	$(CXX) $(LDFLAGS) -o $@ $< $(LIBRARY).a

#dependencies
-include $(SRC:%.cpp=%.d)
