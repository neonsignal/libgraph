// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @mainpage test file.
	* @version 1.0
	* @author Glenn McIntosh
	*/
/** @file libgraph.cpp
	* Main test file.
	*/
// include files
#include "bmp.h"
#include "pnm.h"
#include "dmc.h"
#include "filter.h"
#include "hilbert.h"
#include <iostream>
#include <fstream>

// types
typedef unsigned int uint;

// main
/** Main test function.
	* @param argc number of argments
	* @param argv array of string arguments
	* @return error value (0 = okay)
	*/
int main(int argc, char **argv)
{
	// arguments
	uint reduceColors = 0;
	bool erodeEdges = false;
	uint erodeDegree = 0;
	bool blur = false;
	bool correlate = false;
	uint blurDegree = 0;
	bool tile = false;
	bool tileAlternate = false;
	bool brick = false;
	bool xstitch = false;
	uint dmcSet = 0;
	bool dump = false;
	bool input = false, output = false;

	// parse command line
	graph::Image image;
	for (int iArg = 1; iArg < argc; ++iArg)
	{
		char *arg = argv[iArg];

		// options
		if (arg[0] == '-' || arg[0] == '/')
			switch (arg[1])
			{
			case '?':
			default:
				std::cerr << "Usage(V1.0): " << argv[0] << " [-options] filename" << std::endl;
				std::cerr << "   -?      : help" << std::endl;
				std::cerr << "   -r256   : reduce colors" << std::endl;
				std::cerr << "   -e0     : erode edges" << std::endl;
				std::cerr << "   -b0     : blur" << std::endl;
				std::cerr << "   -c      : auto-correlate" << std::endl;
				std::cerr << "   -t      : tile four-way" << std::endl;
				std::cerr << "   -ta     : tile alternate" << std::endl;
				std::cerr << "   -B      : brick stitch" << std::endl;
				std::cerr << "   -x0     : cross-stitch pattern" << std::endl;
				std::cerr << "   -d      : dump bitmap" << std::endl;
				std::cerr << "   -o file : output" << std::endl;
				return 1;
			case 'r':
				reduceColors = atoi(arg+2);
				if (reduceColors == 0)
					reduceColors = 256;
				break;
			case 'e':
				erodeEdges = true;
				erodeDegree = atoi(arg+2);
				break;
			case 'b':
				blur = true;
				blurDegree = atoi(arg+2);
				break;
			case 'c':
				correlate = true;
				break;
			case 't':
				tile = true;
				if (*(arg+2) == 'a')
					tileAlternate = true;
				break;
			case 'B':
				brick = true;
				break;
			case 'x':
				xstitch = true;
				dmcSet = atoi(arg+2);
				break;
			case 'd':
				dump = true;
				break;
			case 'o':
				output = true;
			}

		// arguments
		else
		{
			if (!output)
				try
				{
					// load image
					{
						std::string s(arg);
						if (s.size() < 4) continue;
						if (s.compare(s.size()-4, 4, ".bmp") == 0)
						{
							graph::BmpImage bmpImage(&image);
							bmpImage.load(arg);
						}
						if (s.compare(s.size()-4, 4, ".ppm") == 0 || s.compare(s.size()-3, 3, "pgm") == 0)
						{
							graph::PnmImage pnmImage(&image);
							pnmImage.load(arg);
						}
						input = true;
					}

					// dump
					if (dump)
					{
						graph::FilterImage filterImage(&image);
						filterImage.dump();
					}

					// reduce the number of colors
					if (reduceColors > 0)
					{
						graph::FilterImage filterImage(&image);
						filterImage.reduceColors(reduceColors);
					}

					// erode edges
					if (erodeEdges)
					{
						graph::FilterImage filterImage(&image);
						filterImage.erodeEdges(erodeDegree);
					}

					// blur
					if (blur)
					{
						graph::FilterImage filterImage(&image);
						filterImage.blur(blurDegree);
					}

					// autocorrelate
					if (correlate)
					{
						graph::FilterImage filterImage(&image);
						filterImage.correlate();
					}

					// change into a tiling
					if (tile)
					{
						graph::FilterImage filterImage(&image);
						filterImage.tile(tileAlternate);
					}

					// change into a brick-stitch
					if (brick)
					{
						graph::FilterImage filterImage(&image);
						filterImage.brick();
					}

					// change into a cross-stitch
					if (xstitch)
					{
						graph::DmcImage dmcImage(&image, dmcSet);
						dmcImage.filter();
					}
				}
				catch (std::exception &error)
				{
					std::cerr << "Error in " << arg << ": " << error.what() << std::endl;
				}
			else
				try
				{
					// save image
					if (input)
					{
						graph::PnmImage pnmImage(&image);
						pnmImage.save(arg);
					}
					output = false;
				}
				catch (std::exception &error)
				{
					std::cerr << "Error in " << arg << ": " << error.what() << std::endl;
				}
		}
	}
	return 0;
}
