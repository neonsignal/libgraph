// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include "hilbert.h"
#include <stdexcept>

namespace graph
{
// 2D hilbert curve class functions
Hilbert::Hilbert(Image &image, Process pProcess)
	: m_image(image), m_pProcess(pProcess)
{
	uint nC = image.size(), nR = image[0].size();

	// if height less than width
	if (m_direction = nR <= nC)
	{
		m_xl = nC;
		m_yl = nR;
	}

	// else width less than height
	else
	{
		m_xl = nR;
		m_yl = nC;
	}
}

void Hilbert::move()
{
	m_pProcess(m_image, m_x, m_y);
	if (m_direction) m_x += m_sign;
	else             m_y += m_sign;
}

// fill a square with dimensions a power of 2
void Hilbert::fill2N()
{
	// initialize
	m_x = 0;
	m_y = 0;
	m_sign = 1;

	// check size)
	if (m_xl != m_yl)
		throw std::out_of_range("area is not square");
	uint l = m_xl;
	if ((l&l-1) != 0)
		throw std::out_of_range("dimensions are not a power of 2");

	// traverse
	recursiveFill2N(l);
}

// fill a rectangle with dimensions not a power of 2
void Hilbert::fill()
{
	// initialize
	m_x = 0;
	m_y = 0;
	m_sign = 1;

	// if simple traversal is possible
	if ((m_yl|m_xl^1)&1)
		recursiveFill(m_xl, m_yl);

	// else split up into two regions at 90 degrees
	else
	{
		uint xl2 = (m_xl+1&~2) / 2;
		if (m_yl-1 < m_xl-xl2) xl2 = m_xl-(m_yl-1);
		recursiveFill(xl2, m_yl);
		move();
		m_direction = !m_direction;
		recursiveFill(m_yl, m_xl-xl2);
	}

	// final move out of area
	move();
}

// recursively fill a square
void Hilbert::recursiveFill2N(uint &l)
{
	// recursion limit
	if (l == 1)
		return;

	// U shaped traversal
	l /= 2;
	m_direction = !m_direction;
	recursiveFill2N(l);
	move();
	m_direction = !m_direction;
	recursiveFill2N(l);
	move();
	recursiveFill2N(l);
	m_direction = !m_direction;
	m_sign = -m_sign;
	move();
	recursiveFill2N(l);
	m_sign = -m_sign;
	m_direction = !m_direction;
	l *= 2;
}

// recursively fill a rectangle
void Hilbert::recursiveFill(uint xl, uint yl)
{
	// recursion limit
	if (xl == 1 && yl == 1)
		return;

	// if very wide, split into two regions
	if (xl >= yl*2)
	{
		uint xl2 = (yl&1 ? xl : xl+1&~2) / 2;
		recursiveFill(xl2, yl);
		move();
		recursiveFill(xl-xl2, yl);
	}

	// else normal U shaped traversal
	else
	{
		uint xl2 = xl / 2;
		uint yl2 = ((yl|xl/2^1)&1 ? yl+1&~2 : yl) / 2;
		m_direction = !m_direction;
		recursiveFill(yl2, xl2);
		move();
		m_direction = !m_direction;
		recursiveFill(xl, yl-yl2);
		m_direction = !m_direction;
		m_sign = -m_sign;
		move();
		recursiveFill(yl2, xl-xl2);
		m_sign = -m_sign;
		m_direction = !m_direction;
	}
}
};
