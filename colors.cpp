// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include "colors.h"
#include <cmath>
typedef unsigned char uchar;

namespace graph
{

// convert RGB to Hilbert color
uint Rgb::getHilbert() const
{
	const uchar pOrient[3][8] =
	{
		{2,1,0,1,2,1,0,1},
		{0,2,1,2,0,2,1,2},
		{1,0,2,0,1,0,2,0},
	};
	const uchar pInvert[3][8] =
	{
		{0,0,3,0,5,6,3,6},
		{0,0,5,0,6,3,5,3},
		{0,0,6,0,3,5,6,5},
	};
	const uchar rotateLeft[3][8] =
	{
		{0,1,2,3,4,5,6,7},
		{0,2,4,6,1,3,5,7},
		{0,4,1,5,2,6,3,7},
	};

	// from orthogonal to Hilbert
	uchar x = static_cast<uchar>(red*255), y = static_cast<uchar>(green*255), z = static_cast<uchar>(blue*255);
	uint hilbert = 0;
	uchar orient = 0, invert = 0;
	uchar bit = 8;
	do
	{
		bit -= 1;
		uchar xyz = (x>>bit&1)<<2 ^ (y>>bit&1)<<1 ^ (z>>bit&1)<<0;
		uchar gray = rotateLeft[orient][xyz^invert];
		hilbert = hilbert<<3 ^ gray;
		hilbert ^= gray>>1 ^ gray>>2;
		invert ^= pInvert[orient][gray];
		orient = pOrient[orient][gray];
	}
	while (bit);
	return hilbert;
}

// convert Hilbert color to RGB
void Rgb::setHilbert(uint hilbert)
{
	const uchar pOrient[3][8] =
	{
		{2,1,0,1,2,1,0,1},
		{0,2,1,2,0,2,1,2},
		{1,0,2,0,1,0,2,0},
	};
	const uchar pInvert[3][8] =
	{
		{0,0,3,0,5,6,3,6},
		{0,0,5,0,6,3,5,3},
		{0,0,6,0,3,5,6,5},
	};
	const uchar rotateRight[3][8] =
	{
		{0,1,2,3,4,5,6,7},
		{0,4,1,5,2,6,3,7},
		{0,2,4,6,1,3,5,7},
	};

	// from Hilbert to orthogonal
	uint x = 0, y = 0, z = 0;
	{
		uchar orient = 0, invert = 0;
		uchar bit = 8*3;
		do
		{
			bit -= 3;
			uchar gray = (hilbert>>bit)&8-1;
			gray ^= gray>>1;
			uchar xyz = rotateRight[orient][gray]^invert;
			x = x<<1 ^ xyz>>2&1;
			y = y<<1 ^ xyz>>1&1;
			z = z<<1 ^ xyz>>0&1;
			invert ^= pInvert[orient][gray];
			orient = pOrient[orient][gray];
		}
		while (bit);
	}
	red = x/255.;
	green = y/255.;
	blue = z/255.;
}

// compare to another color
real Rgb::compare(const Rgb &rgb) const
{
	// Riemersma color difference heuristic
	real distance =
	    (4+(rgb.red+red))*(rgb.red-red)*(rgb.red-red) +
	    8*(rgb.green-green)*(rgb.green-green) +
	    (6-(rgb.red+red))*(rgb.blue-blue)*(rgb.blue-blue);
	/*
		real distance = fabs((real) getHilbert() - (real) rgb.getHilbert()) / (1<<24);
	*/
	return distance/18;
}
};
