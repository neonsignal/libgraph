// Copyright 1998-2016 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once
#include <cmath>

/** @file transform.h
	time/frequency transform functions
	*/
// include files
#include <cstddef>
#include <vector>
#include <valarray>

namespace math
{
using real = double;
using Array = std::vector<real>;
using Matrix = std::vector<std::vector<real>>;

/** Time/frequency transform a vector using decimate-in-time fast Hartley method.
	@param d is the vector to be transformed in place (length a power of two)
	*/
void hartleyTransform(Array &d);
void hartleyTransform(std::valarray<real> &d);
void hartleyNormalize(Array &d);

/** Time/frequency transform a two dimensional vector using decimate-in-time fast Hartley method.
	@param d is the vector to be transformed in place (length a power of two)
	*/
void hartleyTransform(Matrix &d);
void hartleyNormalize(Matrix &d);

/** Auto-correlate a vector.
	@param d is the domain data to be auto-correlated (length a power of 2)
	*/
void autoCorrelate(Array &d);

/** Convolve vectors.
	@param d is the domain data to be convolved (length a power of 2)
	@param f is the filter data (length an odd number)
	*/
void convolve(Array &d, const Array &f);

/** Correlate matrices.
	@param d is the domain data to be cross-correlated (length a power of 2)
	@param f is the filter data (length an odd number)
	*/
void correlate(Matrix &d, const Matrix &f);

/** Convolve matrices.
	@param d is the domain data to be convolved (length a power of 2)
	@param f is the filter data (length an odd number)
	*/
void convolve(Matrix &d, const Matrix &f);

/** Hann window a vector
	@param d is the vector to be windowed in place
	*/
void hann(Array &d);

/** Haar wavelet forward transform
	@param data is the vector to be transformed (length a power of 2)
	*/
void haarTransform0(Array &d);

/** Haar wavelet inverse transform
	@param data is the vector to be transformed (length a power of 2)
	*/
void haarTransform1(Array &d);

/** Daubechies D4 wavelet forward transform
	@param data is the vector to be transformed in place (length a power of 2)
	*/
void d4Wavelet0(Array &d, bool approx = false);

/** Daubechies D4 wavelet inverse transform
	@param data is the vector to be transformed in place (length a power of 2)
	*/
	void d4Wavelet1(Array &d);


/** continuous Daubechies D4 wavelet transform
*/
template<size_t maxScale> class D4WaveletFilter
{
public:
	/** construct a transform
	@param maxScale maximum scale to be returned
	*/
	D4WaveletFilter();
public:
	/** add a new data point
	@param x value
	@param approx output approximation instead of difference (not invertible)
	@result value after transform
	*/
	real operator()(real x);
	// get difference at current scale
	real getApproximation() const {return l0[iScale];}
	size_t scale() const {return iScale;}
	size_t lag() const {return (3<<iScale+1)-3;}
private:
	size_t i;
	size_t iScale;
	real l0[maxScale], l1[maxScale], h1[maxScale];
};

// initialization
template<size_t maxScale> D4WaveletFilter<maxScale>::D4WaveletFilter()
: i(0)
{
	for (iScale = 0; iScale < maxScale; ++iScale)
		l0[iScale] = l1[iScale] = h1[iScale] = 0;
}

// continuous Daubechies D4 wavelet transform
template<size_t maxScale> real D4WaveletFilter<maxScale>::operator()(real x)
{
	// at each scale
	size_t j = i++;
	for (iScale = 0; iScale < maxScale; ++iScale)
	{
		// if odd
		if (j & 1)
		{
			// add in new data
			h1[iScale] = x;

			// lifting steps
			l1[iScale] += sqrt(3.F)*h1[iScale];
			h1[iScale] -= (sqrt(3.F)/4.F)*l1[iScale] + ((sqrt(3.F)-2.F)/4.F)*l0[iScale];
			l0[iScale] -= h1[iScale];
			l0[iScale] *= (sqrt(3.F)-1.F)/2.F;

			// return difference
			return h1[iScale] * (sqrt(3.F)+1.F)/sqrt(2.F);
		}

		// else cascade to next scale with even data
		real x0 = l0[iScale];
		l0[iScale] = l1[iScale];
		l1[iScale] = x;
		x = x0;
		j >>= 1;
	}
	return x;
}

/** continuous LeGall 5/3 wavelet transform
*/
template<size_t maxScale> class LegallWaveletFilter
{
public:
	/** construct a transform
	@param maxScale maximum scale to be returned
	*/
	LegallWaveletFilter();
public:
	/** add a new data point
	@param x value
	@param approx output approximation instead of difference (not invertable)
	@result value after transform
	*/
	real operator()(real v);
	// get difference at current scale
	real getApproximation() const {return l0[iScale];}
	real getDifference() const {return h0[iScale];}
	size_t scale() const {return iScale;}
	size_t lag() const {return (3<<iScale+1)-3-(1<<iScale);}
private:
	size_t i;
	size_t iScale;
	real x0[maxScale], x1[maxScale], h0[maxScale], l0[maxScale];
};

// initialization
template<size_t maxScale> LegallWaveletFilter<maxScale>::LegallWaveletFilter()
: i(0)
{
	for (iScale = 0; iScale < maxScale; ++iScale)
		x0[iScale] = x1[iScale] = h0[iScale] = l0[iScale] = 0;
}

// continuous LeGall 5/3 wavelet transform
template<size_t maxScale> real LegallWaveletFilter<maxScale>::operator()(real x2)
{
	// at each scale
	size_t j = ++i;
	for (iScale = 0; iScale < maxScale; ++iScale)
	{
		// if odd
		if (j & 1)
		{
			// lifting steps
			real h1 = x1[iScale]-(x0[iScale]+x2)/2;
			l0[iScale] = x0[iScale]+(h0[iScale]+h1)/4;
			h0[iScale] = h1;
			x0[iScale] = x2;

			// return difference
			return h0[iScale];
		}

		// else cascade to next scale with even data
		x1[iScale] = x2;
		x2 = l0[iScale];
		j >>= 1;
	}
	return x2;
}
};
