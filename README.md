collection of graphical functions including rudimentary BMP and PNM file processing, a few filters, and creation of brick stitch and cross-stitch patterns

Copyright 2001 Glenn McIntosh

licensed under the GNU General Public License version 3, see [LICENSE.md](LICENSE.md)
