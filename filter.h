// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file filter.h
	* filter functions.
	*/
#pragma once

// include files
#include "colors.h"
#include <vector>

namespace graph
{
typedef std::vector<std::vector<Rgb>> Image;

// filter class
class FilterImage
{
public:
	FilterImage(Image *pImage) : m_image(*pImage) {}
	void reduceColors(uint colors);
	void erodeEdges(uint degree);
	void blur(uint degree);
	void correlate();
	void tile(bool alternate);
	void brick();
	void dump();
private:
	Image &m_image;
};
};
