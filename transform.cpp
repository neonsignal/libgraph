// Copyright 1998-2016 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// include files
#include "transform.h"
#include <functional>
#include <cmath>

namespace math
{
namespace {
// cosine/sine tables
template<typename R> struct CS
{
    R c, s;
    CS &operator+=(const CS &x) {return *this = {c*x.c - s*x.s, s*x.c + c*x.s};}
    const CS operator+(const CS &x) {return CS(*this) += x;}
    CS &operator-=(const CS &x) {return *this = {c*x.c + s*x.s, s*x.c - c*x.s};}
    const CS operator-(const CS &x) {return CS(*this) -= x;}
	const CS reflect() {return {s, c};}
};
constexpr real pi = ::acos(-1);
constexpr CS<real> cs(size_t i) {return {(real) cos(pi/(1<<i)), (real) sin(pi/(1<<i))};}
const CS<real> cs1[] = {cs(0),cs(1),cs(2),cs(3),cs(4),cs(5),cs(6),cs(7),cs(8),cs(9),cs(10),cs(11),cs(12),cs(13),cs(14),cs(15),cs(16),cs(17),cs(18),cs(19),cs(20),cs(21),cs(22),cs(23),cs(24),cs(25),cs(26),cs(27),cs(28),cs(29),cs(30),cs(31)};

// bit function
constexpr size_t ctz(size_t m) {return __builtin_ctz(m);}

// decimate in frequency or in time
#define DFREQ
//#define DTIME

// single layer Hartley computation
inline void hartley(std::valarray<real> &d, size_t i, size_t m, CS<real> *pCs)
{

	// butterfly functions
	auto cross = [&d, &i, &m](size_t w)
	{
		real t0 = d[i+w], t1 = d[i+m+w];
		d[i+w] = t0+t1; d[i+m+w] = t0-t1;
	};
	auto cross2 = [&d, &i, &m](size_t w, CS<real> csw)
	{
		real t0 = d[i+w], t1 = d[i+w+m], t2 = d[i+m-w], t3 = d[i+m-w+m];
#		ifdef DFREQ
		real t4 = t0-t1, t5 = t2-t3;
		d[i+w] = t0+t1; d[i+w+m] = csw.c*t4+csw.s*t5; d[i+m-w] = t2+t3; d[i+m-w+m] = csw.s*t4-csw.c*t5;
#		endif
#		ifdef DTIME
		real t4 = csw.c*t1+csw.s*t3, t5 = csw.s*t1-csw.c*t3;
		d[i+w] = t0+t4; d[i+w+m] = t0-t4; d[i+m-w] = t2+t5; d[i+m-w+m] = t2-t5;
#		endif
	};

	// transform DC, Nyquist
	cross(0);
	if (m&1) return;

	// transform half Nyquist
	cross(m/2);
	if (m&2) return;

	// transform quarter Nyquist frequency components
	cross2(m/4, cs1[2]);
	if (m&4) return;

	// transform other frequency components
	CS<real> csw = *pCs = {1, 0};
	for (size_t w = 1; true; ++w)
	{
		csw += cs1[ctz(m)];
		cross2(w, csw);
		cross2(m/2-w, csw.reflect());
	if (++w & m/4) break;
		csw = *pCs + cs1[ctz(m)-ctz(w)];
		if (w>>1&w&-w) --pCs;
		if (!(w&2)) *++pCs = csw;
		cross2(w, csw);
		cross2(m/2-w, csw.reflect());
	}
}
// single layer Hartley computation
inline void hartley(Array::iterator d, size_t m, CS<real> *pCs)
{

	// butterfly functions
	auto cross = [&d, &m](size_t w)
	{
		real t0 = d[w], t1 = d[m+w];
		d[w] = t0+t1; d[m+w] = t0-t1;
	};
	auto cross2 = [&d, &m](size_t w, CS<real> csw)
	{
		real t0 = d[w], t1 = d[w+m], t2 = d[m-w], t3 = d[m-w+m];
#		ifdef DFREQ
		real t4 = t0-t1, t5 = t2-t3;
		d[w] = t0+t1; d[w+m] = csw.c*t4+csw.s*t5; d[m-w] = t2+t3; d[m-w+m] = csw.s*t4-csw.c*t5;
#		endif
#		ifdef DTIME
		real t4 = csw.c*t1+csw.s*t3, t5 = csw.s*t1-csw.c*t3;
		d[w] = t0+t4; d[w+m] = t0-t4; d[m-w] = t2+t5; d[m-w+m] = t2-t5;
#		endif
	};

	// transform DC, Nyquist
	cross(0);
	if (m&1) return;

	// transform half Nyquist
	cross(m/2);
	if (m&2) return;

	// transform quarter Nyquist frequency components
	cross2(m/4, cs1[2]);
	if (m&4) return;

	// transform other frequency components
	CS<real> csw = *pCs = {1, 0};
	for (size_t w = 1; true; ++w)
	{
		csw += cs1[ctz(m)];
		cross2(w, csw);
		cross2(m/2-w, csw.reflect());
	if (++w & m/4) break;
		csw = *pCs + cs1[ctz(m)-ctz(w)];
		if (w>>1&w&-w) --pCs;
		if (!(w&2)) *++pCs = csw;
		cross2(w, csw);
		cross2(m/2-w, csw.reflect());
	}
}
}

// fast Hartley transform
void hartleyTransform(std::valarray<real> &d)
{
	const size_t n = d.size();
	if (n == 1) return;

#	ifdef DFREQ
	for (size_t i = 0; i < n; i += 2)
	{
		CS<real> csStack[32];
		size_t m = (i|n)&-(i|n);
		while (m > 8)
			hartley(d, i, m>>=1, csStack);
		if (m > 4)
			hartley(d, i, 4, csStack); // unrolled
		if (m > 2)
			hartley(d, i, 2, csStack); // unrolled
		hartley(d, i, 1, csStack); // unrolled
	}
#	endif

	// bit-reversal permutation
	for (size_t m0 = 1, m1 = n>>1; m0 < m1; m0 <<= 1, m1 >>= 1)
		for (size_t i = 0; i < n; i = i+1+(m0|m1) & ~(m0|m1))
			std::swap(d[i+m0], d[i+m1]);

	// Hartley transform each subset
#	ifdef DTIME
	for (size_t i = 2; i <= n; i += 2)
	{
		CS csStack[32];
		auto d.begin() + i - 1;
		size_t m = 1;
	   	hartley(dd -= m, m, csStack); // unrolled
		if (i & (m <<= 1)) continue;
	   	hartley(dd -= m, m, csStack); // unrolled
		if (i & (m <<= 1)) continue;
	   	hartley(dd -= m, m, csStack); // unrolled
		while (!(i & (m <<= 1)))
			hartley(dd -= m, m, csStack);
	}
#endif
}

// fast Hartley transform
void hartleyTransform(Array &d)
{
	const size_t n = d.size();
	if (n == 1) return;

#	ifdef DFREQ
	for (size_t i = 0; i < n; i += 2)
	{
		CS<real> csStack[32];
		auto dd = d.begin() + i;
		size_t m = (i|n)&-(i|n);
		while (m > 8)
			hartley(dd, m>>=1, csStack);
		if (m > 4)
			hartley(dd, 4, csStack); // unrolled
		if (m > 2)
			hartley(dd, 2, csStack); // unrolled
		hartley(dd, 1, csStack); // unrolled
	}
#	endif

	// bit-reversal permutation
	for (size_t m0 = 1, m1 = n>>1; m0 < m1; m0 <<= 1, m1 >>= 1)
		for (size_t i = 0; i < n; i = i+1+(m0|m1) & ~(m0|m1))
			std::swap(d[i+m0], d[i+m1]);

	// Hartley transform each subset
#	ifdef DTIME
	for (size_t i = 2; i <= n; i += 2)
	{
		CS csStack[32];
		auto d.begin() + i - 1;
		size_t m = 1;
	   	hartley(dd -= m, m, csStack); // unrolled
		if (i & (m <<= 1)) continue;
	   	hartley(dd -= m, m, csStack); // unrolled
		if (i & (m <<= 1)) continue;
	   	hartley(dd -= m, m, csStack); // unrolled
		while (!(i & (m <<= 1)))
			hartley(dd -= m, m, csStack);
	}
#endif
}

// hartley normalization after inverse
void hartleyNormalize(Array &d)
{
	const size_t n = d.size();
	for (size_t i = 0; i < n; ++i)
		d[i] /= n;
}

// 2 dimensional fast Hartley transform
void hartleyTransform(Matrix &d)
{
	const size_t n = d.size();
	size_t i, j;

	// transform columns
	for (i = 0; i < n; ++i)
		hartleyTransform(d[i]);

	// transform rows
	for (i = 0; i < n; ++i)
		for (j = i+1; j < n; ++j)
			std::swap(d[i][j], d[j][i]);
	for (i = 0; i < n; ++i)
		hartleyTransform(d[i]);
	for (i = 0; i < n; ++i)
		for (j = i+1; j < n; ++j)
			std::swap(d[i][j], d[j][i]);

	// correct casine separability
	for (i = 1; i < n/2; ++i)
		for (j = 1; j < n/2; ++j)
		{
			real cas = ((d[i][j]+d[n-i][n-j]) - (d[i][n-j]+d[n-i][j]))/2;
			d[i][j] -= cas;
			d[i][n-j] += cas;
			d[n-i][j] += cas;
			d[n-i][n-j] -= cas;
		}
}

// Hartley normalization after inverse
void hartleyNormalize(Matrix &d)
{
	const size_t n = d.size();
	for (size_t i = 0; i < n; ++i)
		for (size_t j = 0; j < n; ++j)
			d[i][j] /= n*n;
}

// auto-correlation
void autoCorrelate(Array &d)
{
	const size_t n = d.size();

	// transform
	hartleyTransform(d);

	// multiply or divide in frequency domain
	d[0] *= d[0];
	for (size_t i = 1; i < n/2; ++i)
		d[i] = d[n-i] = (d[i]*d[i] + d[n-i]*d[n-i])/2;
	d[n/2] *= d[n/2];

	// inverse transform
	hartleyTransform(d);
	hartleyNormalize(d);
}

// 2D convolution kernel
namespace {
static void kernel(Matrix &d, const Matrix &f, const size_t n)
{

	// points unaliased in both domains
	d[0][0] = d[0][0]*f[0][0];
	d[0][n/2] = d[0][n/2]*f[0][n/2];
	d[n/2][0] = d[n/2][0]*f[n/2][0];
	d[n/2][n/2] = d[n/2][n/2]*f[n/2][n/2];

	// remainder
	for (size_t i = 1; i < n/2; ++i)
	{
		real c0, c1, c2, c3;

		// points unaliased in x domain
		c0 = d[0][i]*f[0][n-i] + d[0][n-i]*f[0][i];
		c2 = d[0][i]*f[0][i] - d[0][n-i]*f[0][n-i];
		d[0][i] = (c0+c2)/2;
		d[0][n-i] = (c0-c2)/2;
		c0 = d[n/2][i]*f[n/2][n-i] + d[n/2][n-i]*f[n/2][i];
		c2 = d[n/2][i]*f[n/2][i] - d[n/2][n-i]*f[n/2][n-i];
		d[n/2][i] = (c0+c2)/2;
		d[n/2][n-i] = (c0-c2)/2;

		// points unaliased in y domain
		c0 = d[i][0]*f[n-i][0] + d[n-i][0]*f[i][0];
		c3 = d[i][0]*f[i][0] - d[n-i][0]*f[n-i][0];
		d[i][0] = (c0+c3)/2;
		d[n-i][0] = (c0-c3)/2;
		c0 = d[i][n/2]*f[n-i][n/2] + d[n-i][n/2]*f[i][n/2];
		c3 = d[i][n/2]*f[i][n/2] - d[n-i][n/2]*f[n-i][n/2];
		d[i][n/2] = (c0+c3)/2;
		d[n-i][n/2] = (c0-c3)/2;

		// points aliased in both domains
		for (size_t j = 1; j < n/2; ++j)
		{
			c0 = d[i][j]*f[n-i][n-j] + d[n-i][n-j]*f[i][j] + d[n-i][j]*f[i][n-j] + d[i][n-j]*f[n-i][j];
			c1 = d[i][j]*f[i][j] + d[n-i][n-j]*f[n-i][n-j] - d[n-i][j]*f[n-i][j] - d[i][n-j]*f[i][n-j];
			c2 = d[i][j]*f[n-i][j] - d[n-i][n-j]*f[i][n-j] + d[n-i][j]*f[i][j] - d[i][n-j]*f[n-i][n-j];
			c3 = d[i][j]*f[i][n-j] - d[n-i][n-j]*f[n-i][j] - d[n-i][j]*f[n-i][n-j] + d[i][n-j]*f[i][j];
			d[i][j] = ((c0+c1)+(c2+c3))/4;
			d[n-i][n-j] = ((c0+c1)-(c2+c3))/4;
			d[i][n-j] = ((c0-c1)-(c2-c3))/4;
			d[n-i][j] = ((c0-c1)+(c2-c3))/4;
		}
	}
}
}

// cross correlation
void correlate(Matrix &d, const Matrix &r)
{
	const size_t n = d.size(), m = r.size();

	// align vector sizes
	Array line(n, 0.0);
	Matrix f(n, line);
	for (size_t i = 0; i < m; ++i)
		for (size_t j = 0; j < m; ++j)
			f[i <= m/2 ? i : i-m+n][j <= m/2 ? j : j-m+n] = r[i][j];

	// transform
	hartleyTransform(d);
	hartleyTransform(f);

	// rotate 180 degrees
	for (size_t i = 1; i < n/2; ++i)
	{
		std::swap(f[i][0], f[n-i][0]);
		std::swap(f[i][n/2], f[n-i][n/2]);
		std::swap(f[0][i], f[0][i]);
		std::swap(f[n/2][i], f[n/2][i]);
		for (size_t j = 1; j < n/2; ++j)
		{
			std::swap(f[i][j], f[n-i][n-j]);
			std::swap(f[i][n-j], f[n-i][j]);
		}
	}

	// multiply in frequency domain
	kernel(d, f, n);

	// inverse transform
	hartleyTransform(d);
	hartleyNormalize(d);
}

// convolution
void convolve(Array &d, const Array &r)
{
	const size_t n = d.size(), m = r.size();
	size_t i;

	// align vector sizes
	Array f(n, 0.0);
	for (i = 0; i < m; ++i)
		f[i <= m/2 ? i : i-m+n] = r[i];

	// transform
	hartleyTransform(d);
	hartleyTransform(f);

	// multiply in frequency domain
	d[0] = d[0]*f[0];
	d[n/2] = d[n/2]*f[n/2];
	for (size_t i = 1; i < n/2; ++i)
	{
		real c0 = d[i]*f[i] - d[n-i]*f[n-i];
		real c1 = d[i]*f[n-i] + d[n-i]*f[i];
		d[i] = (c1+c0)/2;
		d[n-i] = (c1-c0)/2;
	}

	// inverse transform
	hartleyTransform(d);
	hartleyNormalize(d);
}

// convolution
void convolve(Matrix &d, const Matrix &r)
{
	const size_t n = d.size(), m = r.size();

	// align vector sizes
	Array line(n, 0.0);
	Matrix f(n, line);
	for (size_t i = 0; i < m; ++i)
		for (size_t j = 0; j < m; ++j)
			f[i <= m/2 ? i : n-m+i][j <= m/2 ? j : n-m+j] = r[i][j];

	// transform
	hartleyTransform(d);
	hartleyTransform(f);

	// multiply in frequency domain
	kernel(d, f, n);

	// inverse transform
	hartleyTransform(d);
	hartleyNormalize(d);
}

// Haar wavelet forward transform
void haarTransform0(Array &d)
{
	const size_t n = d.size();

	// at each scale for each data point
	for (size_t m = 1; m < n; m *= 2)
		for (size_t j = 0; j < n; j += m*2)
		{
			// lifting steps
			real x = d[j];
			real y = d[j+m];
			real s = x+y;
			real t = x-y;
			d[j] = s;
			d[j+m] = t;
		}
}

// Haar wavelet inverse transform
void haarTransform1(Array &d)
{
	const size_t n = d.size();

	// at each scale for each data point
	for (size_t m = n/2; m >= 1; m /= 2)
		for (size_t j = 0; j < n; j += m*2)
		{
			// lifting steps
			real s = d[j];
			real t = d[j+m];
			real x = (s+t)/2;
			real y = (s-t)/2;
			d[j] = x;
			d[j+m] = y;
		}
}

// Daubechies D4 wavelet forward transform
void d4Wavelet0(Array &data, bool approx)
{
	const size_t n = data.size();

	// at each scale
	for (size_t m = 1; m < n;)
	{
		// lifting steps
		for (size_t j = 0; j < n; j += m*2)
			data[j+m*2&n-1] += sqrt(3.F)*data[j+m+m*2&n-1];
		for (size_t j = 0; j < n; j += m*2)
			data[j+m+m*2&n-1] -= (sqrt(3.F)/4.F)*data[j+m*2&n-1] + ((sqrt(3.F)-2.F)/4.F)*data[j];
		for (size_t j = 0; j < n; j += m*2)
			data[j] -= data[j+m+m*2&n-1];
		for (size_t j = 0; j < n; j += m*2)
		{
			data[j] *= (sqrt(3.F)-1.F)/sqrt(2.F);
			data[j+m] = !approx ? data[j+m]*(sqrt(3.F)+1.F)/sqrt(2.F) : data[j];
		}
		m *= 2;
	}
}

// Daubechies D4 wavelet inverse transform
void d4Wavelet1(Array &data)
{
	const size_t n = data.size();

	// at each scale
	for (size_t m = n; m > 1;)
	{
		m /= 2;
		// lifting steps
		for (size_t j = 0; j < n; j += m*2)
		{
			data[j+m] *= (sqrt(3.F)-1.F)/sqrt(2.F);
			data[j] *= (sqrt(3.F)+1.F)/sqrt(2.F);
		}
		for (size_t j = 0; j < n; j += m*2)
			data[j] += data[j+m+m*2&n-1];
		for (size_t j = 0; j < n; j += m*2)
			data[j+m+m*2&n-1] += (sqrt(3.F)/4.F)*data[j+m*2&n-1] + ((sqrt(3.F)-2.F)/4.F)*data[j];
		for (size_t j = 0; j < n; j += m*2)
			data[j+m*2&n-1] -= sqrt(3.F)*data[j+m+m*2&n-1];
	}
}

// Hann window
constexpr real tau = ::acos(-1)*2;
void hann(Array &d)
{
	const size_t n = d.size();
	// filter
	for (size_t i = 0; i < n; ++i)
		d[i] *= 0.5*(1.-cos(tau*i/n));
}
};
