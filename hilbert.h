// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file hilbert.h
	* Hilbert area filling curve.
	*/
#pragma once

// include files
#include <vector>
#include "colors.h"

namespace graph
{
typedef std::vector<std::vector<Rgb>> Image;

// 2D hilbert curve class for image traversal
class Hilbert
{
public:
	typedef void (*Process)(Image &image, const uint &x, const uint &y);
	Hilbert(Image &image, Process pProcess);
	void fill2N();
	void fill();
private:
	void move();
	void recursiveFill2N(uint &l);
	void recursiveFill(uint xl, uint yl);
	Image &m_image;
	void (*m_pProcess)(Image &image, const uint &x, const uint &y);
	uint m_xl, m_yl;
	uint m_x, m_y;
	bool m_direction;
	int m_sign;
};
};
