// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once

namespace graph
{
typedef double real;
typedef unsigned int uint;

// colors
struct Rgb
{
	real red, green, blue;
	uint getHilbert() const;
	void setHilbert(uint hilbert);
	real compare(const Rgb &rgb) const;
};
};
