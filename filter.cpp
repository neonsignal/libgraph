// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include "filter.h"
#include "transform.h"
#include <map>
#include <cmath>
#include <iostream>
#include <iomanip>
namespace graph
{
struct ColorCountItem
{
	ColorCountItem()
	{
		count = 0;
	}
	uint count;
	uint color;
};
typedef std::map<uint, ColorCountItem> ColorCount;

// color filter
static void reduceRecurse(uint colors, uint pixels, ColorCount::iterator color0, ColorCount::iterator color2)
{
	// only one color left to assign to range
	if (colors == 1)
	{
		real average = 0;
		for (ColorCount::iterator color = color0; color != color2; ++color)
			average += (real) color->first * color->second.count;
		average /= pixels;
		for (ColorCount::iterator color = color0; color != color2; ++color)
			color->second.color = static_cast<uint>(average);
		return;
	}

	// split range into two halves (using spread*pixels to compare ranges)
	uint pixels0 = 0, pixels2 = 0;
	ColorCount::iterator color2D = color2;
	--color2D;
	ColorCount::iterator color10D = color0, color12 = color2D;
	uint colors0 = 0, colors2 = 0;
	while (pixels0+pixels2 < pixels && colors0+colors2 < colors)
	{
		if (colors0 <= colors2)
		{
			pixels0 += color10D++->second.count;
			++colors0;
		}
		else
		{
			pixels2 += color12--->second.count;
			++colors2;
		}
	}
	while (pixels0+pixels2 < pixels)
	{
		if ((1.+color10D->first-color0->first)*pixels0 <= (1.+color2D->first-color12->first)*pixels2)
			pixels0 += color10D++->second.count;
		else
			pixels2 += color12--->second.count;
	}
	ColorCount::iterator color1 = color10D;

	// reduce colors for each half of range
	reduceRecurse(colors0, pixels0, color0, color1);
	reduceRecurse(colors2, pixels2, color1, color2);
}

void FilterImage::reduceColors(uint colors)
{
	ColorCount colorCount;
	uint nC = m_image.size(), nR = m_image[0].size();
	uint r, c;

	// count bitmap pixels
	for (c = 0; c < nC; ++c)
		for (r = 0; r < nR; ++r)
			++colorCount[m_image[c][r].getHilbert()].count;

	// reduce colors
	uint pixels = nR * nC;
	reduceRecurse(colors, pixels, colorCount.begin(), colorCount.end());

	// set to new colors
	for (c = 0; c < nC; ++c)
		for (r = 0; r < nR; ++r)
			m_image[c][r].setHilbert(colorCount[m_image[c][r].getHilbert()].color);
}

// edge erosion
void FilterImage::erodeEdges(uint degree)
{
	uint nC = m_image.size(), nR = m_image[0].size();
	uint r, c;

	// make BW image space
	std::vector<bool> line(nR, false);
	std::vector< std::vector<bool> > image(nC, line), invert(nC, line);
	image.insert(image.begin(), nC, line);
	invert.insert(invert.begin(), nC, line);

	// copy to image space
	for (c = 0; c < nC; ++c)
		for (r = 0; r < nR; ++r)
			image[c][r] = m_image[c][r].red+m_image[c][r].green+m_image[c][r].blue > 1.5;

	// blank edges
	for (r = 0; r < nR; ++r)
		image[0][r] = image[nC-1][r] = false;
	for (c = 0; c < nC; ++c)
		image[c][0] = image[c][nR-1] = false;

	// erode until only lines left
	uint count;
	int d0 = 1, d1 = 1;
	do
	{
		count = 0;
		do
		{
			do
			{
				// invert
				int c0 = (d0+d1)/2, r0 = (d0-d1)/2;
				for (c = 1; c < nC-1; ++c)
					for (r = 1; r < nR-1; ++r)
					{
						if (image[c][r] && !image[c-c0][r-r0])
						{
							bool dq = image[c+c0][r+r0], bd = image[c+r0][r+c0], pq = image[c-r0][r-c0];
							bool d = image[c+d0][r+d0], q = image[c+d1][r-d1], b = image[c-d1][r+d1], p = image[c-d0][r-d0];
							if (dq && d && q || bd && !pq && d && (b || dq) || !bd && pq && q && (p || dq))
							{
								++count;
								invert[c][r] = true;
							}
						}
					}

				// erode
				for (c = 0; c < nC; ++c)
					for (r = 0; r < nR; ++r)
						if (invert[c][r])
						{
							invert[c][r] = false;
							image[c][r] = false;
						}

				// next direction
				d1 = -d1;
			}
			while (d1 != d0);

			// next direction
			d0 = -d0;
			d1 = -d1;
		}
		while (d0 != 1);
	}
	while (count > 0 && --degree > 0);

	// copy back from BW image space
	for (c = 0; c < nC; ++c)
		for (r = 0; r < nR; ++r)
		{
			Rgb zero = {0., 0., 0.}, one = {1., 1., 1.};
			m_image[c][r] = image[c][r] ? one : zero;
		}
}

// blur
void FilterImage::blur(uint degree)
{
	uint nC = m_image.size(), nR = m_image[0].size();
	uint r, c;
	const unsigned int n = nC;

	// check size)
	if (nC != nR)
		throw std::out_of_range("area is not square");
	if ((n&n-1) != 0)
		throw std::out_of_range("dimensions are not a power of 2");

	// copy image space
	math::Array line(n, 0.);
	math::Matrix red(n, line), green(n, line), blue(n, line);
	for (c = 0; c < n; ++c)
		for (r = 0; r < n; ++r)
		{
			red[c][r] = m_image[c][r].red;
			green[c][r] = m_image[c][r].green;
			blue[c][r] = m_image[c][r].blue;
		}

	// create simple filter
	math::Matrix filter(n, line);
	filter[0][0] = 1./9;
	filter[0][1] = filter[1][0] = filter[0][n-1] = filter[n-1][0] = 1./9;
	filter[1][1] = filter[1][n-1] = filter[n-1][n-1] = filter[n-1][1] = 1./9;

	// auto-convolve filter
	while (degree--)
		math::convolve(filter, filter);

	// convolve space with filter
	math::convolve(red, filter);
	math::convolve(green, filter);
	math::convolve(blue, filter);

	// copy back image space
	for (c = 0; c < n; ++c)
		for (r = 0; r < n; ++r)
		{
			m_image[c][r].red = red[c][r];
			m_image[c][r].green = green[c][r];
			m_image[c][r].blue = blue[c][r];
		}
}

// auto-correlate
void FilterImage::correlate()
{
	uint nC = m_image.size(), nR = m_image[0].size();
	uint r, c;
	const unsigned int n = nC;

	// check size)
	if (nC != nR)
		throw std::out_of_range("area is not square");
	if ((n&n-1) != 0)
		throw std::out_of_range("dimensions are not a power of 2");

	// copy image space
	math::Array line(n, 0.);
	math::Matrix red(n, line), green(n, line), blue(n, line);
	for (c = 0; c < n; ++c)
		for (r = 0; r < n; ++r)
		{
			red[c][r] = m_image[c][r].red-0.5;
			green[c][r] = m_image[c][r].green-0.5;
			blue[c][r] = m_image[c][r].blue-0.5;
		}

	// auto-correlate
	math::correlate(red, red);
	math::correlate(green, green);
	math::correlate(blue, blue);

	// copy back image space
	for (c = 0; c < n; ++c)
		for (r = 0; r < n; ++r)
		{
			m_image[c][r].red = red[c][r]/red[0][0]/2+0.5;
			m_image[c][r].green = green[c][r]/green[0][0]/2+0.5;
			m_image[c][r].blue = blue[c][r]/blue[0][0]/2+0.5;
		}
}

// tile
void FilterImage::tile(bool alternate)
{
	uint nC = m_image.size(), nR = m_image[0].size();
	uint r, c;

	// copy image space
	math::Matrix red(nC, math::Array(nR, 0.)), green(nC, math::Array(nR, 0.)), blue(nC, math::Array(nR, 0.));
	for (c = 0; c < nC; ++c)
		for (r = 0; r < nR; ++r)
		{
			red[c][r] = m_image[c][r].red;
			green[c][r] = m_image[c][r].green;
			blue[c][r] = m_image[c][r].blue;
		}

	// copy back image space
	for (c = 0; c < nC; ++c)
		for (r = 0; r < nR; ++r)
		{
			real pc, pr;
			uint cc, rr;

			// calculate percentages
			if (c < (nC+1)/2)
			{
				cc = c+nC/2;
				pc = c*2;
			}
			else
			{
				cc = c-(nC+1)/2;
				pc = (nC-c)*2;
			}
			pc /= nC;
			if (r < (nR+1)/2)
			{
				rr = r+nR/2;
				pr = r*2;
			}
			else
			{
				rr = r-(nR+1)/2;
				pr = (nR-r)*2;
			}
			pr /= nR;

			// mix
			if (!alternate)
			{
				real p;
				p = pc*pr;
				m_image[c][r].red = red[c][r]*p;
				m_image[c][r].green = green[c][r]*p;
				m_image[c][r].blue = blue[c][r]*p;
				p = (1.-pc)*pr;
				m_image[c][r].red += red[cc][r]*p;
				m_image[c][r].green += green[cc][r]*p;
				m_image[c][r].blue += blue[cc][r]*p;
				p = pc*(1.-pr);
				m_image[c][r].red += red[c][rr]*p;
				m_image[c][r].green += green[c][rr]*p;
				m_image[c][r].blue += blue[c][rr]*p;
				p = (1.-pc)*(1.-pr);
				m_image[c][r].red += red[cc][rr]*p;
				m_image[c][r].green += green[cc][rr]*p;
				m_image[c][r].blue += blue[cc][rr]*p;
			}
			else
			{
				real p, pdiv;
				pdiv = pc*pr+(1.-pc)*(1.-pr);
				p = pc*pr/pdiv;
				m_image[c][r].red = red[c][r]*p;
				m_image[c][r].green = green[c][r]*p;
				m_image[c][r].blue = blue[c][r]*p;
				p = (1.-pc)*(1.-pr)/pdiv;
				m_image[c][r].red += red[cc][rr]*p;
				m_image[c][r].green += green[cc][rr]*p;
				m_image[c][r].blue += blue[cc][rr]*p;
			}
		}
}

// brick
void FilterImage::brick()
{
	uint nC = m_image.size(), nR = m_image[0].size();
	uint r, c;

	// pair up pixels
	for (c = 0; c < nC; ++c)
		for (r = c&1; ++r < nR; ++r)
		{
			m_image[c][r-1].red = m_image[c][r].red = (m_image[c][r-1].red+m_image[c][r].red)/2;
			m_image[c][r-1].green = m_image[c][r].green = (m_image[c][r-1].green+m_image[c][r].green)/2;
			m_image[c][r-1].blue = m_image[c][r].blue = (m_image[c][r-1].blue+m_image[c][r].blue)/2;
		}
}
// dump bits
void FilterImage::dump()
{
	uint nC = m_image.size(), nR = m_image[0].size();
	uint r, c, b;
	/*
	// dump pixels
		std::cout << std::hex;
		std::cout << '{' << std::endl;
		for (r = 0; r < nR; r += 8)
		{
			std::cout << '\t' << '{';
			for (c = 0; c < nC; ++c)
			{
				uint8_t h = 0;
				uint bMax = 8; if (nR-r < 8) bMax = nR-r;
				for (b = 0; b < bMax; ++b)
					if (m_image[c][r+b].red < 0.5 || m_image[c][r+b].green < 0.5 || m_image[c][r+b].blue < 0.5)
						h |= 1<<b;
				std::cout << "0x" << std::setfill('0') << std::setw(2) << (uint) h << ',';
			}
			std::cout << '}' << ',';
			std::cout << std::endl;
		}
		std::cout << '}' << ';' << std::endl;
	*/

	// dump symbols
	for (c = 0; c < nC; ++c)
	{
		// skip gap
		while (c < nC)
		{
			for (r = 0; r < nR; ++r)
				if (m_image[c][r].red < 0.5 || m_image[c][r].green < 0.5 || m_image[c][r].blue < 0.5)
					break;
			if (r < nR)
				break;
			++c;
		}
		uint c0 = c;

		// skip symbol
		while (c < nC)
		{
			for (r = 0; r < nR; ++r)
				if (m_image[c][r].red < 0.5 || m_image[c][r].green < 0.5 || m_image[c][r].blue < 0.5)
					break;
			if (r == nR)
				break;
			++c;
		}
		uint c1 = c;

		// output symbol
		std::cout << c1-c0 << std::endl;
		std::cout << std::hex;
		std::cout << '{' << std::endl;
		for (r = 0; r < nR; r += 8)
		{
			std::cout << '\t' << '{';
			for (uint cc = c0; cc < c1; ++cc)
			{
				uint8_t h = 0;
				uint bMax = 8;
				if (nR-r < 8) bMax = nR-r;
				for (b = 0; b < bMax; ++b)
					if (m_image[cc][r+b].red < 0.5 || m_image[cc][r+b].green < 0.5 || m_image[cc][r+b].blue < 0.5)
						h |= 1<<b;
				std::cout << "0x" << std::setfill('0') << std::setw(2) << (uint) h << ',';
			}
			std::cout << '}' << ',';
			std::cout << std::endl;
		}
		std::cout << '}' << ';' << std::endl;
	}
}
};
